
class ScheduleEvent(object):

    def __init__(self, ticks_remaining=1):
        self.ticks_remaining = ticks_remaining
        self.source_character = None

    def get_action(self):
        pass

class CharacterEvent(ScheduleEvent):

    def __init__(self, character, ticks_remaining=1):
        super(CharacterEvent, self).__init__(ticks_remaining)
        self.source_character = character

    def get_action(self):
        return self.source_character.get_action()