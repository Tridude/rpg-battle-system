damage_type_fire = "Fire"
damage_type_water = "Water"
damage_type_slashing = "Slash"
damage_type_bashing = "Bash"

class Status(object):

    def __init__(self, name, source_character):
        self.name = name
        self.source_character = source_character


class DamageOverTime(Status):

    def __init__(self, name, source_character, effect, ticks_remaining=0, damage_interval=5):
        super().__init__(name, source_character)
        self.ticks_remaining = ticks_remaining
        self.damage_interval = damage_interval
        self.ticks_to_damage = damage_interval
        self.effect = effect

class Effect(object):

    def __init__(self):
        pass

class DamageEffect(Effect):

    def __init__(self, damage_type, base_damage=0, power_modifier=0.0):
        super().__init__()
        self.damage_type = damage_type
        self.base_damage = base_damage
        self.power_modifier = power_modifier

