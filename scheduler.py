import math
import copy

import character
import events
import status_effects
import gui

class ScheduleQueue(object):

    def __init__(self):
        self.queue = []
        

    def put(self, event):
        index = 0
        for index, queued_event in enumerate(self.queue):
            if queued_event.ticks_remaining > event.ticks_remaining:
                index -= 1
                break
            elif queued_event.ticks_remaining == event.ticks_remaining:
                event.ticks_remaining += 1
        index += 1
        self.queue.insert(index, event)

    def pop(self):
        return self.queue.pop(0)

    def get_next_event(self):
        event = self.pop()
        for queued_event in self.queue:
            queued_event.ticks_remaining -= event.ticks_remaining
        return event

    def remove_character(self, character):
        self.queue = [event for event in self.queue if event.source_character is not character]

    def output(self):
        print(self.queue)
        for event in self.queue:
            print(event.ticks_remaining)

class GameLogic(object):

    def __init__(self):
        self.scheduler = ScheduleQueue()
        self.gui = gui.gui([],[])

    def get_potency(self, source_character, effect):
        potency = skill.base_damage
        

        for attribute_index, multiplier in skill.attribute_multipliers:
            potency += source_character.attributes_current[attribute_index] * multiplier
        return potency

    def get_damage_dealt(self, source_character, target_character, effect):
        if effect.damage_type == status_effects.damage_type_slashing:
            attack_power = source_character.get_slashing_power()
            defense_power = target_character.get_slashing_defense()
        elif effect.damage_type == status_effects.damage_type_bashing:
            attack_power = source_character.get_bashing_power()
            defense_power = target_character.get_bashing_defense()
        elif effect.damage_type == status_effects.damage_type_fire:
            attack_power = source_character.get_fire_power()
            defense_power = target_character.get_fire_defense()
        elif effect.damage_type == status_effects.damage_type_water:
            attack_power = source_character.get_water_power()
            defense_power = target_character.get_water_defense()
        else:
            attack_power = source_character.get_physical_power()
            defense_power = source_character.get_physical_defense()
        
        return int((effect.base_damage + effect.power_modifier * attack_power)  *
            1 / math.log(defense_power, 2))
        # return int( potency *
        #     (
        #         (skill.damage_physical_percentage * (0.06 * target_character.get_physical_defense()
        #             / (1 + 0.06 * target_character.get_physical_defense()))) +
        #         (skill.damage_magical_percentage * (0.06 * target_character.get_magical_defense()
        #             / (1 + 0.06 * target_character.get_magical_defense())))
        #     )
        #     )

    def deal_damage(self, source_character, target_character, damage):
        target_character.attribute_current_health -= damage
        print("%s deals %s damage to %s"%(source_character.name, damage, target_character.name))
        if target_character.attribute_current_health <= 0:
            print("%s is dead"%(target_character.name))
            self.scheduler.remove_character(target_character)
            if target_character in self.allied_characters:
                self.allied_characters.remove(target_character)
            else:
                self.enemy_characters.remove(target_character)
            return True
        return False

    def resolve_action(self, action):
        print("%s uses %s"%(action.source_character.name, action.skill.name))
        for target_character in action.target_characters:
            for status in action.skill.status:
                status = copy.deepcopy(status)
                status.source_character = action.source_character
                target_character.affected_status.append(status)
            for effect in action.skill.effects:
                if isinstance(effect, status_effects.DamageEffect):
                    damage = self.get_damage_dealt(action.source_character, target_character, effect)
                    self.deal_damage(action.source_character, target_character, damage)

    def update_status(self, ticks_since_last_event):
        for character_list in (self.allied_characters, self.enemy_characters):
            for character in character_list:
                dead = False
                status_to_remove = []
                for status in character.affected_status:
                    if isinstance(status, status_effects.DamageOverTime) and not dead:
                        if ticks_since_last_event >= status.ticks_to_damage:
                            # 
                            ticks_count = ticks_since_last_event
                            if status.ticks_remaining is not None and ticks_count > status.ticks_remaining:
                                # status expired before this turn. count only the remaining ticks
                                ticks_count = status.ticks_remaining
                            # how many times the damage effect occured
                            effect_occurences = int(math.floor((ticks_count - status.ticks_to_damage) / status.damage_interval)) + 1
                            status.ticks_to_damage = status.damage_interval - (ticks_count - status.ticks_to_damage - status.damage_interval * (effect_occurences - 1))
                            damage = self.get_damage_dealt(status.source_character, character, status.effect) * effect_occurences
                            print("status %s deals damage %s times"%(status.name, effect_occurences))
                            dead = self.deal_damage(status.source_character, character, damage)
                        else:
                            status.ticks_to_damage -= ticks_since_last_event
                        if status.ticks_remaining is not None:
                            status.ticks_remaining -= ticks_since_last_event
                            if status.ticks_remaining <= 0:
                                status_to_remove.append(status)
                for status in status_to_remove:
                    try:
                        print("Removing %s from %s"%(status.name, character.name))
                        character.affected_status.remove(status)
                    except ValueError as e:
                        # shouldn't happen
                        print("Failed to remove status %s: %s"%(status, e))


    def turn_loop(self):
        current_tick = 0
        while len(self.allied_characters) != 0 and len(self.enemy_characters) != 0:
            print("---------------------------------------------------")
            for allied_character in self.allied_characters:
                print("%s HP: %s"%(allied_character.name, allied_character.attribute_current_health))
                for status in allied_character.affected_status:
                    print("\tStatus %s. Ticks to next damage: %s. Ticks remaining: %s"%(status.name, status.ticks_to_damage, status.ticks_remaining))
            for enemy_character in self.enemy_characters:
                print("%s HP: %s"%(enemy_character.name, enemy_character.attribute_current_health))
                for status in enemy_character.affected_status:
                    print("\tStatus %s. Ticks to next damage: %s. Ticks remaining: %s"%(status.name, status.ticks_to_damage, status.ticks_remaining))
            event = self.scheduler.get_next_event()
            ticks_since_last_event = event.ticks_remaining
            current_tick += ticks_since_last_event
            print("Current tick: %s"%current_tick)
            print("Ticks since last event: %s"%ticks_since_last_event)
            self.update_status(ticks_since_last_event)
            if not (len(self.allied_characters) != 0 and len(self.enemy_characters) != 0):
                break
            action = event.get_action()
            self.resolve_action(action)
            if isinstance(event, events.CharacterEvent):
                self.scheduler.put(events.CharacterEvent(event.source_character, action.skill.character_delay))
            print("---------------------------------------------------\n")
        print("done")

    def main(self):
        self.allied_characters = []
        allied_character_a = character.Character("ally a", self.gui)
        self.allied_characters.append(allied_character_a)
        # allied_character_b = character.Character("ally b", self.gui)
        # self.allied_characters.append(allied_character_b)
        self.enemy_characters = []
        enemy_character_a = character.Character("enemy a", self.gui)
        self.enemy_characters.append(enemy_character_a)
        # enemy_character_b = character.Character("enemy b", self.gui)
        # self.enemy_characters.append(enemy_character_b)
        self.gui.allied_characters = self.allied_characters
        self.gui.enemy_characters = self.enemy_characters
        for allied_character, enemy_character in zip(self.allied_characters, self.enemy_characters):
            self.scheduler.put(events.CharacterEvent(allied_character, 10))
            self.scheduler.put(events.CharacterEvent(enemy_character, 10))

        self.scheduler.output()        
        self.turn_loop()