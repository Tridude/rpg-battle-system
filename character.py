import skills
import status_effects

class Character(object):

    def __init__(self, name, gui):
        self.name = name

        self.attribute_strength = 10
        self.attribute_dexterity = 10
        self.attribute_intelligence = 10
        self.attribute_wisdom = 10
        self.attribute_vitality = 10

        self.attribute_base_health = 100
        self.attribute_base_points = 100
        self.attribute_current_health = 100
        self.attribute_max_health = 100
        self.attribute_current_points = 100
        self.attribute_max_points = 100

        self.skill_list = [skills.Slash(), skills.Bleed()]
        self.affected_status = []

        self.gui = gui

    def get_strength(self):
        return self.attribute_strength

    def get_dexterity(self):
        return self.attribute_dexterity

    def get_intelligence(self):
        return self.attribute_intelligence

    def get_wisdom(self):
        return self.attribute_wisdom

    def get_vitality(self):
        return self.attribute_vitality


    def get_physical_power(self):
        return self.get_strength()

    def get_slashing_power(self):
        return self.get_physical_power()

    def get_bashing_power(self):
        return self.get_physical_power()


    def get_magical_power(self):
        return self.get_intelligence()

    def get_fire_power(self):
        return self.get_magical_power()

    def get_water_power(self):
        return self.get_magical_power()


    def get_physical_defense(self):
        return self.get_vitality()

    def get_slashing_defense(self):
        return self.get_physical_defense()

    def get_bashing_defense(self):
        return self.get_physical_defense()


    def get_magical_defense(self):
        return self.get_vitality() * 0.1 + self.get_wisdom()

    def get_fire_defense(self):
        return self.get_magical_defense()

    def get_water_defense(self):
        return self.get_magical_defense()

    def get_action(self):
        return self.gui.get_action(self)