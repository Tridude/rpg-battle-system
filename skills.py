import character
import status_effects


class Action(object):

    def __init__(self, skill, source_character, target_characters):
        self.skill = skill
        self.source_character = source_character
        self.target_characters = target_characters

class Skill(object):

    def __init__(self):
        self.name = "Skill"
        self.effects = []
        self.status = []
        self.character_delay = 1

        self.target_type = None


class Slash(Skill):

    def __init__(self):
        super().__init__()
        self.name = "Slash"
        self.effects = [
            status_effects.DamageEffect(status_effects.damage_type_slashing, base_damage=10, power_modifier=1.0)
        ]
        self.character_delay = 10

class Bleed(Skill):

    def __init__(self):
        super().__init__()
        self.name = "Bleed"
        self.effects = [
            status_effects.DamageEffect(status_effects.damage_type_slashing, base_damage=3, power_modifier=0.2)
        ]
        self.status = [
            status_effects.DamageOverTime("Bleeding", None,
                status_effects.DamageEffect(status_effects.damage_type_slashing, base_damage=3, power_modifier=0.6),
                ticks_remaining=49, damage_interval=7)
        ]
        self.character_delay = 21
