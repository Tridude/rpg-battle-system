import skills
import random

class gui(object):

    def __init__(self, allied_characters, enemy_characters):
        self.allied_characters = allied_characters
        self.enemy_characters = enemy_characters

    def get_action(self, character):
        if character in self.allied_characters:
            return skills.Action(random.choice(character.skill_list), character, [random.choice(self.enemy_characters)])
        else:
            return skills.Action(random.choice(character.skill_list), character, [random.choice(self.allied_characters)])
